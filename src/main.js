import Vue from 'vue';
import * as VueGoogleMaps from 'vue2-google-maps';
import App from './App.vue';
import router from './router';
import store from './store';
import '@/assets/css/tailwind.css';

Vue.config.productionTip = false;

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyAJcGE0gz4-CUqjrUOlyYppegQcU6FPK74',
  },
});

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
