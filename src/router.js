import Vue from 'vue';
import Router from 'vue-router';
import Valglokaler from './views/Valglokaler.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'valglokaler',
      component: Valglokaler,
    },
  ],
});
