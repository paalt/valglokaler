import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    pollingStations: [],
    postalCodesList: [],
    citiesList: [],
    selectedPostalCode: '',
    selectedAreaName: '',
    searchSource: '',
    dataLoad: true,
  },
  getters: {
    getPollingStations(state) {
      return state.pollingStations;
    },
    getCitiesList(state) {
      return state.citiesList;
    },
    getPostalCodesList(state) {
      return state.postalCodesList;
    },
    getSelectedPostalCode(state) {
      return state.selectedPostalCode;
    },
    getSelectedAreaName(state) {
      return state.selectedAreaName;
    },
    getSearchSource(state) {
      return state.searchSource;
    },
    getDataLoad(state) {
      return state.dataLoad;
    },
  },
  mutations: {
    setPollingStations(state, payload) {
      // eslint-disable-next-line
      state.pollingStations = payload;
    },
    setCitiesList(state, payload) {
      // eslint-disable-next-line
      state.citiesList = payload;
    },
    setPostalCodesLists(state, payload) {
      // eslint-disable-next-line
      state.postalCodesList = payload;
    },
    setSelectedPostalCode(state, payload) {
      // eslint-disable-next-line
      state.selectedPostalCode = payload;
    },
    setSelectedAreaName(state, payload) {
      // eslint-disable-next-line
      state.selectedAreaName = payload;
    },
    setSearchSource(state, payload) {
      // eslint-disable-next-line
      state.searchSource = payload;
    },
    setDataLoad(state, payload) {
      // eslint-disable-next-line
      state.dataLoad = payload;
    },
  },
  actions: {
    setPollingStations({ commit }, payload) {
      commit('setPollingStations', payload);
    },
    setCitiesList({ commit }, payload) {
      commit('setCitiesList', payload);
    },
    setPostalCodesLists({ commit }, payload) {
      commit('setPostalCodesLists', payload);
    },
    setSelectedPostalCode({ commit }, payload) {
      commit('setSelectedPostalCode', payload);
    },
    setSelectedAreaName({ commit }, payload) {
      commit('setSelectedAreaName', payload);
    },
    setSearchSource({ commit }, payload) {
      commit('setSearchSource', payload);
    },
    setDataLoad({ commit }, payload) {
      commit('setDataLoad', payload);
    },
  },
});
